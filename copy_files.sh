#!/usr/bin/env bash

if [ "$1" != "" ] && [ "$1" != "files/" ] && [ -d "$1" ]
then
	echo moving old config files to ~/backup...
	mkdir -p ~/backup
	#Find the directories to back up
	find ~/.config/* -maxdepth 0 -type d -print | grep "echo $(ls $1/)" | xargs -I % cp -rf % ~/backup/

	echo moving $1 config files to ~/.config/
	cp -rf $1/* ~/.config/

else
	echo "Please specify a valid path!"
fi
