#!/usr/bin/env bash

if [ -d "files/$1" ] && [ "$1" != "" ]
then
	echo "Installing on $1/Debian"
	echo 'Continue? (y/n):'
	read selection

	if [ "$selection" == "y" ] || [ "$selection" == "Y" ]
	then
		#refresh apt
		sudo apt update
		sudo apt upgrade -y

		sudo apt install libxcb1-dev libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev libxcb-icccm4-dev libyajl-dev libstartup-notification0-dev libxcb-randr0-dev libev-dev libxcb-cursor-dev libxcb-xinerama0-dev libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev autoconf xutils-dev dh-autoreconf unzip git -y
		sudo apt install git nitrogen rofi python-pip binutils gcc make pkg-config fakeroot cmake python-xcbgen xcb-proto libxcb-ewmh-dev wireless-tools libiw-dev libasound2-dev libpulse-dev libcurl4-openssl-dev libmpdclient-dev pavucontrol -y
		sudo apt install build-essential libffi-dev gtk-doc-tools libmount-dev i3 -y
		wget http://ftp.gnome.org/pub/gnome/sources/glib/2.58/glib-2.58.1.tar.xz
		tar -xf glib-2.58.1.tar.xz
		rm glib-2.58.1.tar.xz
		cd glib-2.58.1
		./autogen.sh
		make -j8
		sudo make install
		cd ..
		rm -rf glib-2.58.1


		git clone https://github.com/unix121/i3wm-themer.git
		cd i3wm-themer
		./install_debian.sh
		cd ..
		rm -rf i3wm-themer

		./copy_files.sh files/$1
		./install_programs.sh
		nitrogen --set-scaled ~/.config/nitrogen/003.png

		else
			echo "Installation was aborted!"
		fi

else
echo "No argument was given! Specify shana or kanna!"
fi
