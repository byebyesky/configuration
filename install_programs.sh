#!/usr/bin/env bash

GREEN='\e[32m'
WHITE='\e[39m'
RED='\e[31m'

echo "$1"

if [ "$1" == "--dir" ]
then
PROGRAMFOLDER=$1
else
PROGRAMFOLDER=~/Programs/
if [ ! -f $PROGRAMFOLDER ]
then
mkdir -p $PROGRAMFOLDER
fi
fi

echo "INSTALLING TO $PROGRAMFOLDER"
echo "YOU CAN SPECIFY A DIRECTORY WITH --dir"
echo "Is that the right directory? (y/n)"
read selection
if [ "$selection" == "y" ] || [ "$selection" == "Y" ] 
then

if [[ `command -v xcompmgr` == ""  ]]
then
	sudo apt install xcompmgr
else
	echo -e "${WHITE}xcompmgr is already installed"
fi

#Install catimg
if [[ `command -v catimg` == ""  ]]
then
	git clone https://github.com/posva/catimg.git
	cd catimg
	cmake .
	sudo make install
	cd ..
	echo -e "${GREEN}catimg succesfully installed"
else
	echo -e "${WHITE}catimg is already installed!"
fi

if [[ `command -v neofetch` == ""  ]]
then
	cp ./programs/neofetch $PROGRAMFOLDER
	echo -e "${GREEN}neofetch succesfully installed"
else
	echo -e "${WHITE}neofetch is already installed"
fi


echo -e "${WHITE}Do you want to install termite? (y/n)"
read selection
if [ "$selection" == "y" ] || [ "$selection" == "Y" ] 
then
#Install termite
if [[ `command -v termite` == ""  ]]
then
	wget https://raw.githubusercontent.com/Corwind/termite-install/master/termite-install.sh
	chmod +x termite-install.sh
	./termite-install.sh
	rm termite-install.sh
	echo -e "${GREEN}termite succesfully installed"
else
	echo -e "${WHITE}termite is already installed!"
fi
fi

if [ "$1" != "--no-delete" ]
then
	rm -rf catimg
fi

else
echo -e "${RED}Install aborted by user!"
fi

echo -e "${WHITE}"
