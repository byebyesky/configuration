#!/usr/bin/env bash

if [ -d "files/$1" ] && [ "$1" != "" ]
then
	echo "Installing on $1/Ubuntu"
	echo 'Continue? (y/n):'
	read selection

	if [ "$selection" == "y" ] || [ "$selection" == "Y" ]
	then
		#refresh apt
		sudo apt update
		sudo apt upgrade -y

		sudo apt install git -y
		git clone https://github.com/unix121/i3wm-themer.git
		cd i3wm-themer
		./install_ubuntu.sh
		cd ..
		rm -rf i3wm-themer

		./copy_files.sh files/$1
		./install_programs.sh
		nitrogen --set-scaled ~/.config/nitrogen/003.png

		else
			echo "Installation was aborted!"
		fi

else
echo "No argument was given! Specify shana or kanna!"
fi
